package fr.univ.projet;


import fr.univ.projet.algorithms.Colorization;
import fr.univ.projet.algorithms.D_Satur;
import fr.univ.projet.algorithms.Sequential;
import fr.univ.projet.algorithms.SimulatedAnnealing;
import fr.univ.projet.benchmark.BenchMaker;
import fr.univ.projet.graph.Graph;
import fr.univ.projet.graph.RandomGraph;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);

        LinkedList<Graph> g = new LinkedList<>();
        g.add(new RandomGraph(60, 0.4));
        g.add(new RandomGraph(100, 0.4));
        g.add(new RandomGraph(300, 0.4));
        BenchMaker bm = new BenchMaker(g);
        bm.fullBench();

    }
}
