package fr.univ.projet.graph;

public class RandomGraph extends Graph {

    /**
     * Graph generation with Gilbert model
     * @param n node number
     * @param p arc probability
     */
    public RandomGraph(int n, double p) {
        super(n);
        gilbert(n, p);
    }

    public RandomGraph(int n, double p, boolean oriented) {
        super(n, oriented);
        gilbert(n, p);
    }

    /**
     * Graph generation with Erdös-Rényi model
     * @param n node numbers
     * @param m arc numbers
     */
    public RandomGraph(int n, int m) {
        super(n);
        erdosRenys(n, m);
    }

    public RandomGraph(int n, int m, boolean oriented) {
        super(n, oriented);
        erdosRenys(n, m);
    }

    private void gilbert(int n, double p) {
        if (p > 1)
            return;

        int src = 1;
        int finalNode = -1;

        while (src < n) {
            double r = Math.random();
            finalNode += 1 + (int) Math.floor(Math.log(1 - r) / Math.log(1 - p));

            while (finalNode >= src && src < n) {
                finalNode -= src;
                ++src;
            }

            if (src < n) {
                this.addArc(src, finalNode);
            }
        }
    }

    private void erdosRenys(int n, int m) {
        int[] replace = new int[(int) binomial(n, 2)];

        for (int i = 0; i < m; ++i) {
            int r = (int) Math.floor(Math.random() * (binomial(n, 2) - 1));
            int[] arcBij = bijection(r);

            if (arcNotExists(arcBij))
                addArc(arcBij[0], arcBij[1]);
            else {
                int[] tmp = bijection(replace[r]);
                addArc(tmp[0], tmp[1]);
            }

            arcBij = bijection(i);
            if (arcNotExists(arcBij))
                replace[r] = i;
            else
                replace[r] = replace[i];
        }

    }

    private int[] bijection(int i) {
        double v = 1 + Math.floor((-0.5 + Math.sqrt(0.25 + 2 * i)));
        double w = i - (v * (v - 1)) / 2d;

        return new int[]{(int) v, (int) w};
    }

    /**
     * Calculates the binomial coefficient
     *
     * @param n the fixed set of elements
     * @param k the subset of elements
     * @return
     */
    private long binomial(int n, int k) {
        if (k > n - k)
            k = n - k;

        long b = 1;
        for (int i = 1, m = n; i <= k; i++, m--)
            b = b * m / i;
        return b;
    }

    private boolean arcNotExists(int[] arcs) {
        Node n = this.nodes.get(arcs[0]);
        if (n != null) {
            return !n.hasSuccessor(arcs[1]);
        }
        return true;
    }
}
