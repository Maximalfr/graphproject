package fr.univ.projet.graph;

import java.util.ArrayList;
import java.util.List;

/**
 * @author william
 * Object representation of a node
 */
public class Node implements Comparable<Node> {

    public final int id;
    /**
     * All arcs contained in the list have this node as srcNode.
     */
    private final List<Arc> successors = new ArrayList<>();
    private boolean mark = false;
    /**
     * 0 is not a color. Color stats to 1.
     */
    private int color = 0;

    /**
     * Construct a node with the given id
     * @param id
     */
    public Node(int id) {
        this.id = id;
    }

    /**
     * Search if the node id is a successor of the node
     * @param id potential node successor id
     * @return true if it's a successor otherwise false
     */
    public boolean hasSuccessor(int id) {
        for (Arc arc : this.successors) {
            if (arc.getFinalNode().id == id)
                return true;
        }
        return false;
    }

    /**
     * Add an arc to the node. It needs to have this node in srcNode
     * @param arc
     */
    public void addSucc(Arc arc) {
        if (arc.getSrcNode().id == this.id && !this.successors.contains(arc) && !arc.getFinalNode().equals(this))
            this.successors.add(arc);
    }

    public void setMark(boolean b) {
        this.mark = b;
    }

    public void setColor(int color) { this.color = color; }

    public int getColor() { return this.color; }

    public boolean getMark() {
        return this.mark;
    }

    public List<Arc> getSuccessors() {
        return this.successors;
    }

    /**
     * Returns all successor nodes in an arrayList
     * @return
     */
    public List<Node> getSuccessorNodes() {
        List<Node> list = new ArrayList<>();
        for (Arc a : this.successors) {
            list.add(a.getFinalNode());
        }
        return list;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(id);

        if (this.color != 0)
            sb.append("(").append(this.color).append(")");

        sb.append(": ");

        if (this.successors.size() == 0)
            return sb.append("Ø").toString();


        for (int i = 0; i < this.successors.size(); ++i) {
            sb.append(this.successors.get(i).getFinalNode().id);

            if (i + 1 < this.successors.size())
                sb.append(", ");
        }
        return sb.toString();
    }

    @Override
    public int compareTo(Node n) {
        return this.id - n.id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(Node.class))
            return ((Node) obj).id == this.id;
        return false;
    }
}

