package fr.univ.projet.graph;

/**
 * @author william
 */
public class Arc {

    private Node srcNode;
    private Node finalNode;

    public Arc(Node x, Node y) {
        this.srcNode = x;
        this.finalNode = y;
    }

    @Override
    public String toString() {
        return this.srcNode.id + " ==> " + this.finalNode.id;
    }

    public Node getSrcNode() {
        return this.srcNode;
    }

    public Node getFinalNode() {
        return this.finalNode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(this.getClass())) {
            Arc a = (Arc) obj;
            return this.srcNode.equals(a.srcNode) && this.finalNode.equals(a.finalNode);
        }
        return false;
    }
}
