package fr.univ.projet.graph;

/**
 * @author william
 */
public class SmallWorld extends Graph {

    public SmallWorld(int n, int d) {
        super(n);

        if (d < 1 || d > Math.floor((n - 1) / 2d)) {
            return;
        }

        for (int i = 1; i <= n; ++i) {
            for (int j = i + 1; j < i + d; j++) {
                addArc(i, j % n);
            }
        }
    }

    public SmallWorld(int n, int d, double p) {
        this(n, d);

        for (int i = 1; i <= n; ++i) {
            //TODO
        }
    }

}
