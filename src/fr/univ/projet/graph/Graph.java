package fr.univ.projet.graph;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.HashMap;

/**
 * @author william
 * Object representation of a graph
 */
public class Graph {

    /**
     * Contains the node with its ID as key.
     */
    protected HashMap<Integer, Node> nodes = new HashMap<>();

    /**
     * Add the possibility to make not oriented graph
     */
    protected final boolean oriented;

    /**
     * Construct the graph with a csv file.
     * The file needs to be formatted in this way:
     * node1, node2
     * node1 has an arc pointed at node2 and node2 at node1
     * ex:
     * 5,4
     * 1,2
     *
     * @param file
     * @throws IOException
     */
    public Graph(String file) throws IOException {
        this.oriented = false;
        graphConstructionByFile(file);
    }

    /**
     * Construct the graph with a csv file.
     * The file needs to be formatted in this way:
     * node1, node2
     * if oriented is true
     * node1 has an arc pointed at node2
     * if oriented is false
     * node1 has an arc pointed at node2 and node2 at node1
     * ex:
     * 5,4
     * 4,5
     * 1,2
     *
     * @param file
     * @param oriented set an oriented graph is oriented is true
     * @throws IOException
     */
    public Graph(String file, boolean oriented) throws IOException {
        this.oriented = oriented;
        graphConstructionByFile(file);
    }

    /**
     * Build a generated graph with k nodes. It contains only nodes, no nodes are connected.
     *
     * @param k number of nodes
     */
    public Graph(int k) {
        this.oriented = false;
        graphConstructionByNumber(k);
    }

    /**
     * Build a generated graph with k nodes. It contains only nodes, no nodes are connected.
     *
     * @param k        number of nodes
     * @param oriented set an oriented graph is oriented is true
     */
    public Graph(int k, boolean oriented) {
        this.oriented = oriented;
        graphConstructionByNumber(k);
    }

    /**
     * Construct the graph with a csv file.
     * The file needs to be formatted in this way:
     * node1, node2
     * node1 has an arc pointed at node2
     * ex:
     * 5,4
     * 4,5
     * 1,2
     *
     * @param file
     * @throws IOException
     */
    private void graphConstructionByFile(String file) throws IOException {
        Reader in = new FileReader(file);
        Iterable<CSVRecord> records = CSVFormat.RFC4180.parse(in);
        int line = 1;
        for (CSVRecord record : records) {
            try {
                int x = Integer.parseInt(record.get(0).trim());
                addNode(x);

                if (record.size() >= 2) {
                    int y = Integer.parseInt(record.get(1).trim());
                    addNode(y);
                    addArc(x, y);
                }

                ++line;

            } catch (NumberFormatException e) {  // if the csv contains a string, it can't be parsed to an integer
                System.err.println(e);
                System.err.println("line " + line);

            } catch (ArrayIndexOutOfBoundsException e) {
                System.err.println("malformed csv, the csv must be formatted with 2 columns x,y");
                System.err.println("line " + line);
            }
        }
    }

    /**
     * Build a generated graph with k nodes. It contains only nodes, no nodes are connected.
     *
     * @param k number of nodes
     */
    private void graphConstructionByNumber(int k) {
        for (int id = 0; id < k; ++id)
            this.nodes.put(id, new Node(id));
    }

    /**
     * Add a node with the given id
     *
     * @param id
     * @return
     */
    public Node addNode(int id) {
        Node n = getNode(id);
        if (n == null) {// Avoid redundancies
            n = new Node(id);
            this.nodes.put(id, n);
        }
        return n;
    }

    /**
     * Add an arc between node x and node y.
     * By default, the arc is not oriented,
     * but if the graph is set as oriented then the arc is oriented x => y.
     *
     * @param x source node id
     * @param y final node id
     */
    public void addArc(int x, int y) {
        Node nodeX = getNode(x);
        Node nodeY = getNode(y);

        if (nodeX == null || nodeY == null)
            return;

        if (this.oriented) {
            if (!nodeX.hasSuccessor(y))
                nodeX.addSucc(new Arc(nodeX, nodeY));
        } else {
            if (!nodeX.hasSuccessor(y) && !nodeY.hasSuccessor(x)) {
                nodeX.addSucc(new Arc(nodeX, nodeY));
                nodeY.addSucc(new Arc(nodeY, nodeX));
            }
        }
    }

    public void travel() {
        for (Node n : this.nodes.values()) {
            n.setMark(false);
        }

        for (Node n : this.nodes.values()) {
            if (!n.getMark()) {
                deep(n, 0);
            }
        }
    }


    public void deep(Node node, int index) {
        node.setMark(true);

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < index; ++i) {
            sb.append("-");
        }
        sb.append(node.id);
        System.out.println(sb);

        for (Node finalNode : node.getSuccessorNodes()) {
            if (!finalNode.getMark()) {
                deep(finalNode, index + 1);
            }
        }
    }

    /**
     * Export the graph into a csv file.
     * The filename depending on the class name.
     */
    public void export() {
        StringBuilder buff = new StringBuilder("Source,Target\n");
        String sep = ",";

        for (Node n : this.nodes.values()) {
            if (n.getSuccessors().size() == 0)
                buff.append(n.id).append("\n");

            for (Arc a : n.getSuccessors()) {
                buff.append(a.getSrcNode().id)
                        .append(sep)
                        .append(a.getFinalNode().id)
                        .append("\n");
            }
        }
        File outputFile = new File((this.getClass()) + ".csv");
        FileWriter out;
        try {
            out = new FileWriter(outputFile);
            out.write(buff.toString());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Graph duplicate() {
        Graph g = new Graph(0, this.oriented);
        for (Node n : this.nodes.values()) {
            g.addNode(n.id);
        }
        for (Node n : this.nodes.values()) {
            for (Node finalNode : n.getSuccessorNodes()) {
                g.addArc(n.id, finalNode.id);
            }
        }
        return g;
    }

    public boolean isOriented() {
        return this.oriented;
    }

    /**
     * Get the node with the given id or return null if it doesn't exists
     *
     * @param id node id
     * @return the node object or null
     */
    public Node getNode(int id) {
        return this.nodes.getOrDefault(id, null);
    }

    /**
     * Return the hashmap containing all graph nodes.
     * The hashmap is formed with the node id as key and the node object as value
     */
    public HashMap<Integer, Node> getNodes() {
        return this.nodes;
    }

    /**
     * Return the number of nodes
     */
    public int getSize() {
        return this.nodes.size();
    }

    /**
     * Return the highest color contained in the graph.
     */
    public int getChromaticNumber() {
        int tmp = 0;
        for (Node n : getNodes().values()) {
            if (tmp < n.getColor())
                tmp = n.getColor();
        }
        return tmp;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Graph: ");
        if (this.oriented)
            sb.append("oriented");
        else
            sb.append("unoriented");
        sb.append("\n");

        for (Node n : this.nodes.values())
            sb.append(n.toString()).append("\n");
        return sb.toString();
    }
}
