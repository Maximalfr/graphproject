package fr.univ.projet.benchmark;

import fr.univ.projet.algorithms.Colorization;
import fr.univ.projet.algorithms.D_Satur;
import fr.univ.projet.algorithms.Sequential;
import fr.univ.projet.algorithms.SimulatedAnnealing;
import fr.univ.projet.graph.Graph;

import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class BenchMaker {

    private final LinkedList<Graph> graphs = new LinkedList<>();
    private boolean headerSet = false;

    public BenchMaker(Graph graph) {
        this.graphs.add(graph);
    }

    public BenchMaker(List<Graph> graphs) {
        this.graphs.addAll(graphs);
    }

    public void fullBench() {
        fullBench(null);
    }

    public void fullBench(String filepath) {
        List<Colorization> cList = new LinkedList<>();
        System.out.println("\n##############RUN############\n");


        for (Sequential.Method m : Sequential.Method.values())
            cList.add(new Sequential(null, m));

        cList.add(new SimulatedAnnealing(null));
        cList.add(new D_Satur(null));

        if (filepath == null)
            benchColorization(cList);
        else
            export(filepath, cList);
        System.out.println("\n##############END__RUN############\n");
    }

    private void benchColorization(List<Colorization> colorizations) {

        for (Graph graph : this.graphs) {
            System.out.println("############################");
            System.out.println("Nodes: " + graph.getNodes().size());
            System.out.println("############################");
            for (Colorization c : colorizations) {
                c.setGraph(graph.duplicate());
                System.out.println("======================\n");
                if (c.getClass().equals(Sequential.class))
                    System.out.println(c.getClass().getSimpleName() + ": " + ((Sequential) c).getMethod());
                else
                    System.out.println(c.getClass().getSimpleName() + ":");

                System.out.println("Graph with " + c.getGraph().getSize() + " nodes\n");
                c.run();
                System.out.println("Chromatic numbers: " + c.getGraph().getChromaticNumber() + "\n" +
                        "Execution Time: " + c.getExecutionTime(true) / 1e6 + "ms\n");
            }
        }
    }

    private void export(String filename, List<Colorization> colorizations) {
        final String separator = ",";

        StringBuilder header = new StringBuilder("Nodes: ").append(separator);
        for (Graph graph : this.graphs) {
            header.append(graph.getNodes().size()).append(separator);
        }

        StringBuilder color = new StringBuilder();
        StringBuilder exTime = new StringBuilder();

        for (Colorization c : colorizations) {
            String name;
            if (c.getClass().equals(Sequential.class)) {
                name = c.getClass().getSimpleName() + " " + ((Sequential) c).getMethod().toString();
            } else {
                name = c.getClass().getSimpleName();
            }
            color.append(name).append(" (color)").append(separator);
            exTime.append(name).append(" (time)").append(separator);

            for (Graph graph : this.graphs) {
                c.setGraph(graph.duplicate());
                c.run();
                color.append(c.getGraph().getChromaticNumber()).append(separator);
                exTime.append(c.getExecutionTime()).append(separator);
            }
            color.append("\n");
            exTime.append("\n");
        }

        try {
            FileWriter fw = new FileWriter(filename);
            fw.write(header.append("\n").append(color).append("\n").append(exTime).toString());
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
