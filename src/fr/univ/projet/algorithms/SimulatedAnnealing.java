package fr.univ.projet.algorithms;

import fr.univ.projet.graph.Graph;
import fr.univ.projet.graph.Node;

import java.util.LinkedList;
import java.util.Random;

/**
 * @author william
 */
public class SimulatedAnnealing extends Colorization {

    private final double HEAT;
    private final double COOLING;
    private final Random random;

    public SimulatedAnnealing(Graph graph) {
        super(graph);
        this.COOLING = 0.24;
        this.HEAT = 16;
        this.random = new Random();
    }

    public SimulatedAnnealing(Graph graph, double heat, double cooling) {
        super(graph);
        this.HEAT = heat;
        this.COOLING = cooling;
        this.random = new Random();
    }

    @Override
    protected Graph algorithm() {
        LinkedList<Node> x = new LinkedList<>(this.graph.getNodes().values());
        LinkedList<Node> xp;
        double temperature = this.HEAT;
        int f = function(x);


        while (temperature > 0.05) {
            xp = permutation(x);
            while (x != xp) {
                if (function(xp) <= function(x)) {
                    x = xp;
                    if (function(x) < f) {
                        f = function(x);
                        xp = x;
                    }
                } else {
                    double p = this.random.nextDouble();
                    if (p < Math.exp(-(function(xp) - function(x)) / temperature))
                        x = xp;
                }
            }
            temperature = temperature * this.COOLING;
        }

        return this.graph;
    }

    private int function(LinkedList<Node> list) {
        int maxColor = 0;
        for (Node n : list) {
            n.setColor(0); // need that to reset the node
            this.coloring(n);
            if (n.getColor() > maxColor)
                maxColor = n.getColor();
        }
        return maxColor;
    }

    private LinkedList<Node> permutation(LinkedList<Node> list) {
        int a = this.random.nextInt(list.size());
        int b = this.random.nextInt(list.size());
        LinkedList<Node> newList = (LinkedList<Node>) list.clone();

        Node tmp = newList.get(b);
        newList.set(b, newList.get(a));
        newList.set(a, tmp);

        return newList;
    }
}
