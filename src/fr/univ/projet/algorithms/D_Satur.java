package fr.univ.projet.algorithms;

import fr.univ.projet.algorithms.utils.DegreeSorter;
import fr.univ.projet.graph.Arc;
import fr.univ.projet.graph.Graph;
import fr.univ.projet.graph.Node;

import java.util.HashSet;

/**
 * @author Thomas Kastner
 * @date 17/03/2019
 */

public class D_Satur extends Colorization {

    public D_Satur(Graph graph) {
        super(graph);
    }


    @Override
    public Graph algorithm() {
        //color the maximal degree node then color the node minimizing d_Sat
        getMaximalDegreeNodeUncolored().setColor(1);

        while (!isAllColored()) {
            this.coloring(findDsatMin());
        }
        return graph;
    }


    protected boolean isAllColored() {
        for (Node n : graph.getNodes().values()) {
            if (n.getColor() == 0) return false;
        }
        return true;
    }


    protected Node findDsatMin() {

        Node theChosenOne = null;


        for (Node n : this.graph.getNodes().values()) {
            if (n.getColor() != 0)
                continue;

            if (theChosenOne == null) {
                theChosenOne = n;
                continue;
            }
            if (d_Sat(n) < d_Sat(theChosenOne)) {
                theChosenOne = n;
                continue;
            }

            if (d_Sat(n) == d_Sat(theChosenOne) && n.getSuccessors().size() > theChosenOne.getSuccessors().size())
                theChosenOne = n;
        }
        return theChosenOne;
    }


    protected int d_Sat(Node n) {
        HashSet<Integer> listColors = new HashSet<>();
        for (Arc k : n.getSuccessors()) {
            if (k.getFinalNode().getColor() != 0) {
                listColors.add(k.getFinalNode().getColor());
            }
        }
        return listColors.size();
    }


    protected Node getMaximalDegreeNodeUncolored() {
        DegreeSorter dg = new DegreeSorter(this.graph, DegreeSorter.DES);

        while (!dg.isEmpty()) {
            Node n = dg.pop();
            if (n == null)
                return null;
            if (n.getColor() == 0) {
                return n;
            }
        }
        return null;
    }
}
