package fr.univ.projet.algorithms.utils;

import fr.univ.projet.graph.Graph;
import fr.univ.projet.graph.Node;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author william
 * The DegreeSorter sorts nodes by degree, ascending or descending.
 */
public class DegreeSorter {

    /**
     * Key : Node degree
     * Value : LinkedList containing nodes corresponding to node degree
     *
     */
    private TreeMap<Integer, LinkedList<Node>> nodesByDegree;

    public static final int ASC = 0;
    public static final int DES = 1;

    /**
     * Creates an empty degreeSorter. By default, it will be ordered by asc
     */
    public DegreeSorter() {
        this.nodesByDegree = new TreeMap<>();
    }

    /**
     * Creates an empty degree sorter ordered by the value in parameter
     * The ordering possibility are DegreeSorter.ASC or DegreeSorter.DES
     * @param order the order ASC or DES
     */
    public DegreeSorter(int order) {
        switch (order) {
            case ASC:
                this.nodesByDegree = new TreeMap<>();
                break;
            case DES:
                this.nodesByDegree = new TreeMap<>(Comparator.reverseOrder());
                break;
        }
    }

    /**
     * Creates a DegreeSorter with graph's nodes. It will be ordered by asc
     */
    public DegreeSorter(Graph graph) {
        this();
        addGraph(graph);
    }

    /**
     * Creates a DegreeSorter with graph's nodes. It will be ordered by the value in parameter
     * The ordering possibility are DegreeSorter.ASC or DegreeSorter.DES
     */
    public DegreeSorter(Graph graph, int order) {
        this(order);
        addGraph(graph);
    }

    /**
     * Add all the graph's nodes into the degree sorter.
     * @param graph
     */
    public void addGraph(Graph graph) {
        this.nodesByDegree.clear();

        for (Node n : graph.getNodes().values()) {
            int size = n.getSuccessors().size();

            if (this.nodesByDegree.containsKey(size))
                this.nodesByDegree.get(size).add(n);
            else {
                LinkedList<Node> list = new LinkedList<>();
                list.add(n);
                this.nodesByDegree.put(size, list);
            }
        }
    }

    /**
     * Retrieves and removes the first element of the degree sorted, or returns null if it doesn't contains node anymore.
     * If multiple nodes are on the same degree, the method pops all nodes before changing degree (it doesn't mean anything)
     * @return
     */
    public Node pop() {
        Map.Entry<Integer, LinkedList<Node>> entry = this.nodesByDegree.firstEntry();
        if (entry == null)
            return null;
        Node node = entry.getValue().pollFirst();
        if (entry.getValue().size() == 0) {
            this.nodesByDegree.pollFirstEntry();
        }
        return node;
    }

    public boolean isEmpty() {
        return this.nodesByDegree.size() == 0;
    }


}
