package fr.univ.projet.algorithms;

import fr.univ.projet.graph.Graph;
import fr.univ.projet.graph.Node;

import java.util.TreeSet;

/**
 * @author william
 * Abstract class for all colorization algorithm.
 * Provides an unified code and some interesting result (execution time for the moment)
 */
public abstract class Colorization {

    private long executionTime = -1;
    protected Graph graph;
    private boolean lock = false;

    public Colorization(Graph graph) {
        this.graph = graph;
    }

    /**
     * Execute the algorithm.
     * @return the colored graph
     */
    public Graph run() {
        this.lock = true;
        Graph coloredGraph;
        long start = System.nanoTime();
        coloredGraph = algorithm();
        executionTime = (System.nanoTime() - start);
        this.lock = false;
        return coloredGraph;
    }

    /**
     * Give the last execution time in millisecond. If the algorithm hasn't be executed once, return -1
     * @return
     */
    public int getExecutionTime() {
        return (int) (executionTime / 1e6);
    }

    /**
     * Give the last execution time in milli or nanosecond. If the algorithm hasn't be executed once, return -1
     * @param nano return a execution time in nano second is it set to true otherwise it is millisecond
     * @return
     */
    public double getExecutionTime(boolean nano) {
        return nano ? this.executionTime : (this.executionTime / 1e6);
    }


    /**
     * Color the node taking into account its successors
     * Return nothing because it takes the node reference
     *
     * @param n the node to color
     */
    protected void coloring(Node n) {
        TreeSet<Integer> colors = new TreeSet<>(); // TreeSet because it's ordered
        for (Node finalNode : n.getSuccessorNodes()) {
            if (finalNode.getColor() != 0) {
                colors.add(finalNode.getColor());
            }
        }
        int nodeColor = 1;
        for (int c : colors) {
            if (nodeColor == c)
                ++nodeColor;
        }

        n.setColor(nodeColor);
    }

    /**
     * This method contains the algorithm needed to process the coloration
     * It needs to return a colored graph.
     * @return
     */
    protected abstract Graph algorithm();

    public Graph getGraph() {
        return this.graph;
    }

    public void setGraph(Graph graph) {
        if (!this.lock)
            this.graph = graph;
    }
}
