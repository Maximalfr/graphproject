package fr.univ.projet.algorithms;

import fr.univ.projet.algorithms.utils.DegreeSorter;
import fr.univ.projet.graph.Arc;
import fr.univ.projet.graph.Graph;
import fr.univ.projet.graph.Node;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

/**
 * @author william
 * Suite of sequential coloring algorithm
 */
public class Sequential extends Colorization {

    private final Method method;

    /**
     * Different methods for the sequential algorithm
     */
    public enum Method {
        FIRST_FIT_SEQUENTIAL,
        LARGEST_FIRST_SEQUENTIAL,
        SMALLEST_LAST_SEQUENTIAL
    }

    public Sequential(Graph graph, Method method) {
        super(graph);
        this.method = method;
    }

    @Override
    protected Graph algorithm() {
        switch (this.method) {
            case FIRST_FIT_SEQUENTIAL:
                firstFit();
                break;
            case LARGEST_FIRST_SEQUENTIAL:
                largestFirst();
                break;
            case SMALLEST_LAST_SEQUENTIAL:
                smallestLast();
                break;
        }

        return this.graph;
    }

    /**
     * First fit sequential algorithm
     */
    private void firstFit() {
        TreeSet<Node> nodes = new TreeSet<>(this.graph.getNodes().values());

        for (Node n : nodes)
            this.coloring(n);

    }

    /**
     * Largest first sequential algorithm
     */
    private void largestFirst() {
        DegreeSorter degreeSorter = new DegreeSorter(graph, DegreeSorter.DES);

        for (Node n = degreeSorter.pop(); n != null; n = degreeSorter.pop()) {
            this.coloring(n);
        }
    }

    /**
     * Smallest Last Sequential algorithm
     * <p>
     * Look at this for a graph example (graph3.csv): https://www.chegg.com/homework-help/version-sequential-coloring-algorithm-smallest-last-approach-chapter-8-problem-41e-solution-9781133608424-exc
     */
    private void smallestLast() {
        Graph g = this.graph;
        LinkedList<Node> orderedNode = new LinkedList<>();

        for (Node n = getBiggestDegree(g, orderedNode); n != null; n = getBiggestDegree(g, orderedNode)) {
            orderedNode.add(n);
            n.setMark(true);
        }

        for (Node n : orderedNode) {
            this.coloring(n);
        }
    }

    /**
     * Gives the node with the biggest degree. This method rejects all nodes in excludedNode and doesn't count marked one.
     *
     * @param graph         The graph which contains all nodes
     * @param excludedNodes Nodes to exclude
     * @return the biggest unmarked node
     */
    private Node getBiggestDegree(Graph graph, List<Node> excludedNodes) {
        int max = -1;
        Node biggest = null;

        for (Node n : graph.getNodes().values()) {
            if (n.getMark() || excludedNodes.contains(n))
                continue;

            int tmp = 0;
            for (Arc a : n.getSuccessors()) {
                if (!a.getFinalNode().getMark()) {
                    ++tmp;
                }
            }
            if (tmp > max) {
                max = tmp;
                biggest = n;
            }
        }
        return biggest;
    }

    public Sequential.Method getMethod() {
        return this.method;
    }
}
