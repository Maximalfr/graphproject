# Graph project

### Colorization
To add a new algorithm to color a graph, you need to extend your class with the Colorization one. You need to override the algoritm() method with your algorith.

#### Create a Graph
```java
// You have the possibility to create a unoriented graph (by default)
Graph g = new Graph(4);
// Or
Graph g = new Graph(4, false);

// It's possible to construct a oriented graph
Graph g = new Graph(4, true);
```
Please note that all randomGraph objects in the project have the same behavior.

#### Use Colorization object
```java
// Create the object
Colorization c = new NewAlgo(graph, number);

// The run method calls algorith() (the overrided one)
Graph coloredGraph = c.Run();

// It's possible to get the last execution time
c.getExecutionTime();
```
